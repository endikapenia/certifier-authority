#!/bin/bash

VARS=${CERTS_PATH}/vars

EASYRSA_REQ_COUNTRY=${EASYRSA_REQ_COUNTRY:-""}
EASYRSA_REQ_PROVINCE=${EASYRSA_REQ_PROVINCE:-""}
EASYRSA_REQ_CITY=${EASYRSA_REQ_CITY:-""}
EASYRSA_REQ_ORG=${EASYRSA_REQ_ORG:-""}
EASYRSA_REQ_EMAIL=${EASYRSA_REQ_EMAIL:-""}
EASYRSA_REQ_OU=${EASYRSA_REQ_OU:-""}
EASYRSA_KEY_SIZE=${EASYRSA_KEY_SIZE:-""}
DEFAULT_CA_NAME=${DEFAULT_CA_NAME:-"Viwom Video Marketing S.L"}

init_CA(){
    if [ ! -f "$VARS" ]; then
        cp -R /usr/share/easy-rsa/* ${CERTS_PATH} 


        
cat > $VARS << EOL
EASYRSA_PKI="${CERTS_PATH}pki"
EASYRSA_REQ_COUNTRY="${EASYRSA_REQ_COUNTRY}"
EASYRSA_REQ_PROVINCE="${EASYRSA_REQ_PROVINCE}"
EASYRSA_REQ_CITY="${EASYRSA_REQ_CITY}"
EASYRSA_REQ_ORG="${EASYRSA_REQ_ORG}"
EASYRSA_REQ_EMAIL="${EASYRSA_REQ_EMAIL}"
EASYRSA_REQ_OU="${EASYRSA_REQ_OU}"
EASYRSA_KEY_SIZE="${EASYRSA_KEY_SIZE}"
EOL

    cd ${CERTS_PATH} && source ./vars && ./easyrsa --batch init-pki && ./easyrsa --batch gen-dh && ./easyrsa --batch --req-cn="$DEFAULT_CA_NAME" build-ca nopass && ./easyrsa --batch gen-crl

    chown ${USERNAME}: -R ${CERTS_PATH} 
    
    fi
}

init_CA