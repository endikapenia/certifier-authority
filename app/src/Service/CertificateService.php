<?php

namespace App\Service;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Entity\Cert;

class CertificateService
{
    const DEFAULT_PATH = 'pki/';
    const SAVE_CLIENT_CERT_PATH = 'client/';
    const SAVE_SERVER_CERT_PATH = 'server/';
    const CA_CERT_PATH = 'pki/ca.crt';
    const DH_PATH = 'pki/dh.pem';
    const CRL_PATH = 'pki/crl.pem';
    const RSA_COMMAND_PATH = 'CERTS_PATH';
    const DAYS = '1825';

    //Types of cert
    const SERVER_TYPE = 0;
    const CLIENT_TYPE = 1;
    const CA_TYPE = 2;
    const CRL_TYPE = 3;

    private $commandPath;

    public function __construct()
    {
        $this->commandPath = $_ENV[self::RSA_COMMAND_PATH];
    }

    public function readCert(string $cert)
    {
        return openssl_x509_parse($cert);
    }

    public function createCert(string $name, string $altNames, int $type): string 
    {
        if ($type == self::SERVER_TYPE)
        {
            return $this->createServerCert($name,$this->formatingAlternativeNames($altNames));
        } 
        else if ($type == self::CLIENT_TYPE)
        {
            return $this->createClientCert($name,$this->formatingAlternativeNames($altNames));
        }
    }

    public function createClientCert(string $name)
    {
        $options = [
            '--req-cn='.$name,
        ];
        $options = array_merge($options,
            [
            'build-client-full',
            $name,
            'nopass'
            ]);
        return $this->process($options);
    }


    public function download(string $cn)
    {
        $zipname = '/tmp/'.$cn. '.zip';
        if (file_exists($zipname)) {
                unlink($zipname);
            }
        $files = $this->listCert();
        $file = $files[$cn];
        $zip = new \ZipArchive;
        $zip->open($zipname, \ZipArchive::CREATE);
        if ($file->getCertType() == Cert::TYPE_SERVER) {
            $zip->addFile($this->commandPath . self::CRL_PATH);
        }
        $zip->addFile($file->getPrivate());
        $zip->addFile($file->getPublic());
        $zip->addFile($file->getRequest());        
        if (!is_null($file->getP12())){
            $zip->addFile($file->getP12());
        }
        $zip->addFile($this->commandPath . self::CA_CERT_PATH);
        $zip->close();
        return $zipname;
    }


    public function isRevoke(string $serial): bool
    {
        return array_key_exists($serial,$this->parseCRL());
    }

    public function createServerCert(string $name, string $altNames)
    {
        $options = [
            '--req-cn='.$name,
        ];
        if (strlen($altNames) > 0){
            array_push($options,'--subject-alt-name='.$altNames);
        }
        $options = array_merge($options,
            [
            'build-server-full',
            $name,
            'nopass'
            ]);
        return $this->process($options);
    }

    public function downloadCA(): string
    {
        return $this->commandPath . self::CA_CERT_PATH;
    }

    public function downloadDH(): string
    {
        return $this->commandPath . self::DH_PATH;
    }

    public function downloadCRL(): string
    {
        return $this->commandPath . self::CRL_PATH;
    }

    public function listCert(): array
    {
        return $this->getCerts();
    }

    public function revoke(string $cn)
    {
        $this->revokeProcess($cn);
        $this->regenerateCRL();
    }

    public function delete(string $cn)
    {
        $files = $this->listCert();
        $file = $files[$cn];
        if (!is_null($file->getPrivate()) && file_exists($file->getPrivate())){
            unlink($file->getPrivate());
        }
        if (!is_null($file->getPublic()) && file_exists($file->getPublic())){
            unlink($file->getPublic());
        }
        if (!is_null($file->getRequest()) && file_exists($file->getRequest())){
            unlink($file->getRequest());
        }
        return;
    }

    private function getCerts()
    {
        $finder = new Finder();
        $finder->files()->in($this->commandPath . self::DEFAULT_PATH);
        $finder->name('*.crt')->name('*.key')->name('*.req')->name('*.p12');
        $finder->notName('ca.*')->notName('dh.pem');
        
        $files = [];
        foreach ($finder as $file)
        {
            $name = explode('.',$file->getFileName())[0];
            $cert = (array_key_exists($name,$files)) ? $files[$name] : new Cert() ;
            $cert->setName($name);
            $cert->setCa($this->downloadCA());
            $ext = $file->getExtension();
            if ($ext == 'key'){                
                $cert->setPrivate($file->getPathName());
            }else if ($ext == 'req'){
                $cert->setRequest($file->getPathName());
                $info = $this->readCert($file->getContents());
            }else if ($ext == 'p12'){
                $cert->setP12($file->getPathName());
            }else{
                // $files[$name]['info'] = $this->readCert($file->getContents());
                $info = $this->readCert($file->getContents());
                $cert->setPublic($file->getPathName());
                $cert->setCn($info['subject']['CN']);
                $altNames = (isset($info['extensions']['subjectAltName'])) ? $info['extensions']['subjectAltName'] : '' ;
                $certType = (isset($info['extensions']['extendedKeyUsage']) && $info['extensions']['extendedKeyUsage']=='TLS Web Server Authentication') ? Cert::TYPE_SERVER : Cert::TYPE_CLIENT;
                $cert->setAltNames($altNames);
                $date = new \DateTime();
                $cert->setValidTo($date->setTimestamp($info['validTo_time_t']));
                $cert->setSerialNumber($info['serialNumberHex']);
                $cert->setIssuer($info['issuer']['CN']);
                $cert->setStatus(
                    ($this->isRevoke($cert->getSerialNumber())) ? Cert::STATUS_REVOKE : Cert::STATUS_ACTIVE
                ); 
                $cert->setCertType($certType);
            }
            $files[$name] = $cert;  
        }
        return $files;
    }
    

    private function formatingAlternativeNames(string $altNames): string
    {
        if (strlen($altNames) == 0){
            return '';
        }
        $names = explode(',',$altNames);
        $response = '';
        foreach ($names as $name)
        {
            $response .= 'DNS:'.$name.',';
        }
        $response = trim($response,',');
        return $response;
    }

    private function process(array $options)
    {
        $defaultOptions = [$this->commandPath . 'easyrsa','--batch','--days='. self::DAYS];
        $options = array_merge($defaultOptions,$options);
        $command = new Process($options);
        $command->run();
        // executes after the command finishes
        if (!$command->isSuccessful()) {
            throw new ProcessFailedException($command);
        }
        return $command->getOutput();
    }

    private function parseCRL()
    {
        $crl = $this->commandPath . self::CRL_PATH;
        $options = ['openssl', 'crl', '-inform', 'PEM', '-text', '-noout', '-in', $crl];
        $command = new Process($options);
        $command->run();
        // executes after the command finishes
        if (!$command->isSuccessful()) {
            throw new ProcessFailedException($command);
        }
        preg_match_all("/Serial\sNumber:\s\w+/m",$command->getOutput(),$match_all);
        $match = [];
        foreach($match_all[0] as $pattern)
        {
            $serialNumber = str_replace("Serial Number: ",'',$pattern);
            $match[$serialNumber] = $serialNumber;
        }
        return $match;
    }

    private function revokeProcess(string $cn)
    {
        $options = [$this->commandPath . 'easyrsa','--batch','revoke', $cn];
        $command = new Process($options);
        $command->run();
        // executes after the command finishes
        if (!$command->isSuccessful()) {
            throw new ProcessFailedException($command);
        }
        return $command->getOutput();
    }

    public function regenerateCRL()
    {
        $options = [$this->commandPath . 'easyrsa','--batch','gen-crl'];
        $command = new Process($options);
        $command->run();
        // executes after the command finishes
        if (!$command->isSuccessful()) {
            throw new ProcessFailedException($command);
        }
        return $command->getOutput();
    }
}