<?php

namespace App\Entity;

class Cert
{

    const STATUS_REVOKE = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_SERVER = 0;
    const TYPE_CLIENT = 1;

    private $name;
    private $private;
    private $public;
    private $p12;
    private $request;
    private $ca;
    private $cn;
    private $certType;
    private $status;
    private $altNames;
    private $validTo;
    private $serialNumber;
    private $issuer;


    public function __construct()
    {
        
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }


    /**
     * Set the value of private
     *
     * @return  self
     */ 
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * Get the value of private
     */ 
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Set the value of public
     *
     * @return  self
     */ 
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get the value of public
     */ 
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set the value of p12
     *
     * @return  self
     */ 
    public function setP12($p12){
        $this->p12 = $p12;

        return $this;
    }

    /**
     * Get the value of p12
     */ 
     public function getP12(){
        return $this->p12;
    }

    /**
     * Set the value of ca
     *
     * @return  self
     */ 
    public function setCa($ca)
    {
        $this->ca = $ca;

        return $this;
    }

    /**
     * Get the value of ca
     */ 
    public function getCa()
    {
        return $this->ca;
    }

    /**
     * Set the value of info
     *
     * @return  self
     */ 
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get the value of info
     */ 
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set the value of cn
     *
     * @return  self
     */ 
    public function setCn($cn)
    {
        $this->cn = $cn;

        return $this;
    }

    /**
     * Get the value of cn
     */ 
    public function getCn()
    {
        return $this->cn;
    }

    /**
     * Set the value of altnames
     *
     * @return  self
     */ 
    public function setAltNames($altNames)
    {
        $this->altNames = $altNames;

        return $this;
    }

    /**
     * Get the value of altNames
     */ 
    public function getAltNames()
    {
        return $this->altNames;
    }

    /**
     * Set the value of validTo
     *
     * @return  self
     */ 
    public function setValidTo($validTo)
    {
        $this->validTo = $validTo;

        return $this;
    }

    /**
     * Get the value of validTo
     */ 
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * Set the value of serialNumber
     *
     * @return  self
     */ 
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get the value of serialNumber
     */ 
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set the value of issuer
     *
     * @return  self
     */ 
    public function setIssuer($issuer)
    {
        $this->issuer = $issuer;

        return $this;
    }

    /**
     * Get the value of issuer
     */ 
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of certType
     *
     * @return  self
     */ 
    public function setCertType($certType)
    {
        $this->certType = $certType;

        return $this;
    }

    /**
     * Get the value of certType
     */ 
    public function getCertType()
    {
        return $this->certType;
    }

    /**
     * Set the value of request
     *
     * @return  self
     */ 
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get the value of request
     */ 
    public function getRequest()
    {
        return $this->request;
    }
}