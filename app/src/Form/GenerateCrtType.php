<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Service\CertificateService;

class GenerateCrtType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cert_type',ChoiceType::class,[
                'label' => 'Certificate type',
                'attr' => ['class' => 'form-control'],
                'choices' => [
                    'Server' => CertificateService::SERVER_TYPE,
                    'Client' => CertificateService::CLIENT_TYPE
                ]
            ])
            ->add('common_name',TextType::class,[
                'label' => 'Common name',
                'attr' => ['class' => 'form-control','placeholder' => "Insert common name (Example: viewed.app)"],                
            ])
            ->add('alt_names',TextType::class,[
                'label' => 'Alternative names',
                'attr' => ['class' => 'form-control','placeholder' => "Insert alternative names separate with comma (Example: *.viewed.app,*.viewed.video)"],
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
