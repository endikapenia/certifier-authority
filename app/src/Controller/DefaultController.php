<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\GenerateCrtType;
use App\Form\GenerateReqType;
use App\Service\CertificateService;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default", methods={"GET"})
     */
    public function index(CertificateService $crtServ)
    {
        $certs = $crtServ->listCert();
        return $this->render('default/index.html.twig', [
            'certs' => $certs,
        ]);
    }

    /**
     * @Route("/test", name="test", methods={"GET"})
     */
    public function test(CertificateService $crtServ)
    {
        $certs = $crtServ->listCert();
        $response = new JsonResponse($certs);
        return $response;
    }

    /**
     * @Route("/create-crt", name="create_crt", methods={"GET","POST"})
     */
    public function createRequestCRT(Request $request, CertificateService $crtServ)
    {
        $form = $this->createForm(GenerateCrtType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();            
            $altNames = ($data['alt_names']) ? $data['alt_names'] : '';
            $output = $crtServ->createCert($data['common_name'],$altNames,$data['cert_type']);
            (strlen($output) > 0) ? $this->addFlash('warning', '¡Oops error ocurred at create cert!') : $this->addFlash('success', 'This certificate create successfully') ;            
            return $this->redirect($this->generateUrl('default'));  
        }

        return $this->render('default/crt.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/delete", name="delete", methods={"GET"})
     */
    public function removeCert(Request $request, CertificateService $crtServ)
    {
        $cert = $request->query->get('cert');
        $crtServ->delete($cert);
        $this->addFlash('success', 'This certificate '.$cert.' is remove successfully');
        return $this->redirect($this->generateUrl('default'));  
    }

    /**
     * @Route("/create-csr", name="create_csr")
     */
    public function createRequestCSR()
    {
        $form = $this->createForm(GenerateReqType::class);
        return $this->render('default/csr.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/revoke", name="revoke", methods={"GET"})
     */
    public function revoke(Request $request, CertificateService $crtServ)
    {
        $cert = $request->query->get('cert');
        $crtServ->revoke($cert);
        $this->addFlash('success', 'This certificate '.$cert.' is revoke successfully');
        return $this->redirect($this->generateUrl('default'));  
    }

     /**
     * @Route("/download", name="download", methods={"GET"})
     */
    public function download(Request $request, CertificateService $crtServ)
    {
        $cert = $request->query->get('cert');
        $response = new BinaryFileResponse($crtServ->download($cert));
        // To generate a file download, you need the mimetype of the file
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        // Set the mimetype with the guesser or manually
        if($mimeTypeGuesser->isSupported()){
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($crtServ->downloadCA()));
        }else{
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'text/plain');
        }      
        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $cert.'.zip'
        );        
        return $response;
    }

    /**
     * @Route("/download/{file}", name="download_ca", methods={"GET"}, requirements={
     *      "file"="ca|dh|crl"
     * })
     */
    public function downloadCA(string $file,CertificateService $crtServ)
    {
        $download = 'ca.crt';
        $downloadFile = $crtServ->downloadCA();
        if ($file == 'ca') {
            $download = 'ca.crt';
            $downloadFile = $crtServ->downloadCA();
        } else if ($file == 'dh') {
            $download = 'dh.pem';
            $downloadFile = $crtServ->downloadDH();
        } else if ($file == 'crl') {
            $download = 'crl.pem';
            $crtServ->regenerateCRL();
            $downloadFile = $crtServ->downloadCRL();
        }
        
        $response = new BinaryFileResponse($downloadFile);
        // To generate a file download, you need the mimetype of the file
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        // Set the mimetype with the guesser or manually
        if($mimeTypeGuesser->isSupported()){
            // Guess the mimetype of the file according to the extension of the file
            $response->headers->set('Content-Type', $mimeTypeGuesser->guess($crtServ->downloadCA()));
        }else{
            // Set the mimetype of the file manually, in this case for a text file is text/plain
            $response->headers->set('Content-Type', 'text/plain');
        }      
        // Set content disposition inline of the file
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $download
        );        
        return $response;
    }
}
