PHPCPD_FILES ?= src/
PHPMD_FILES ?= src/
LINT_FILES ?= src/

#CSFIXER_FILES ?= src/
APP_COMMAND = docker-compose run --rm app

#DOCKER PARAMETES
DOCKER_IMAGE_BASE = registry.gitlab.com/sxbuilder/php:latest
DOCKER_IMAGE_NAME = certifier-authority
DOCKER_IMAGE_VERSION ?= latest

# DOCKER REGISTRIES
DOCKER_PRIVATE_REGISTRY = registry.gitlab.com/sxbuilder/

.PHONY: *

test: codeception ## PhpUnitTest

packagin: update_base ## Create production docker image
	docker build --target PROD -t "${DOCKER_PRIVATE_REGISTRY}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .

update_base: ## Upgrade docker base image
	docker pull ${DOCKER_IMAGE_BASE}

build: update_base ## Build project for develop
	docker-compose build

tags: packagin ## Generate tags
	docker tag ${DOCKER_PRIVATE_REGISTRY}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION} ${DOCKER_PRIVATE_REGISTRY}${DOCKER_IMAGE_NAME}:latest

push: tags ## Upload docker image to private registry
	docker push ${DOCKER_PRIVATE_REGISTRY}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}
	docker push ${DOCKER_PRIVATE_REGISTRY}${DOCKER_IMAGE_NAME}:latest

up-d: build ## Up container
	docker-compose up

up: up-d ps

ps: ## List containers
	docker-compose ps

composer_install:
	${APP_COMMAND} composer install --no-scripts --no-suggest --no-progress --no-interaction --prefer-dist --optimize-autoloader

composer_update:
	${APP_COMMAND} composer update --no-interaction

composer_require:
	${APP_COMMAND} composer require

composer_require_dev:
	${APP_COMMAND} composer require --dev

require_dev: composer_require_dev ## Composer require dev
require: composer_require ## Composer require
compr: composer_require ## Composer require
compi: composer_install ## Composer install
install: composer_install ## Composer install
compu: composer_update ## Composer update
update: composer_update ## Composer update

phpunit:
	${APP_COMMAND} php -n vendor/bin/phpunit ${PHPUNIT_OPTIONS}

csfixer:
	${APP_COMMAND} php -n vendor/bin/php-cs-fixer fix --using-cache=yes --cache-file=.php_cs.cache ${CSFIXER_FILES}

csfixer_nocache:
	${APP_COMMAND} php -n vendor/bin/php-cs-fixer fix ${CSFIXER_FILES}

down: ## Docker compose down
	docker-compose down

kill: ## Docker compose kill
	docker-compose kill

cc: ## Cache clear
	${APP_COMMAND} php -n bin/console cache:clear

restart: kill up ##Restart

phpcpd:
	${APP_COMMAND} php -n vendor/bin/phpcpd ${PHPCPD_FILES}

phpmd:
	${APP_COMMAND} php -n vendor/bin/phpmd ${PHPMD_FILES} text phpmd.ruleset.xml

codeception: ## Test codeception
	${APP_COMMAND} codecept run --steps


attach: ## Attach
	${APP_COMMAND} bash

#lint:
#	docker-compose run --rm app find ${LINT_FILES} | grep "\.php" | xargs -n 1 php -l

mr: csfixer phpmd test ## Execute test

pre_commit:
	${APP_COMMAND} hooks/pre-commit.php ${COMMIT_FILES}

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
