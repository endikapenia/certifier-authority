FROM registry.gitlab.com/sxbuilder/php:latest AS DEVELOP

ARG USER_UID=1000

ENV APP_ENV=dev \
    USER_UID=${USER_UID} \
    PATH=$PATH:/usr/local/bin \
    APP_NGINX_DIR=${APP_DIR}/public \
    CERTS_PATH=/certified-authority/

COPY entrypoint.sh ${INIT_SCRIPTS}

RUN apk --no-cache add shadow easy-rsa && usermod -u ${USER_UID} ${USERNAME} \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && mkdir -p ${CERTS_PATH} \
    && chown ${USERNAME}: -R ${APP_DIR} \
    && rm -Rf ${APP_DIR}/var/cache 

ENV PATH=${APP_DIR}/vendor/bin:$PATH

## BUILD PRODUCTION STEP
FROM registry.gitlab.com/sxbuilder/php:latest AS PROD

ENV APP_ENV=prod \
    PATH=$PATH:/usr/local/bin \
    APP_NGINX_DIR=${APP_DIR}/public \
    CERTS_PATH=/certified-authority/

COPY app/ ${APP_DIR}
COPY entrypoint.sh ${INIT_SCRIPTS}

RUN apk --no-cache add easy-rsa && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && mkdir -p ${CERTS_PATH} \
    && mkdir -p ${APP_DIR}/var \
    && chown ${USERNAME}: -R ${APP_DIR} \
    && cd ${APP_DIR} \
    && su ${USERNAME} -c "PATH=$PATH:/usr/local/bin && composer install --no-scripts --no-suggest --no-progress --no-interaction --prefer-dist --optimize-autoloader --no-dev" \
    && chown ${USERNAME}: -R ${APP_DIR} \
    && rm -Rf ${APP_DIR}/var/cache 
